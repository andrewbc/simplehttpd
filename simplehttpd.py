import io
import os
import sys
import html
import urllib.parse

from http import server
from collections import OrderedDict

__version__ = '0.1.0'

class handler(server.SimpleHTTPRequestHandler):
    """Extendable Simple HTTP request handler with GET and HEAD commands.

    This differs from server.SimpleHTTPRequestHandler only in that
    extendability is injected through utilization of user-settable 
    class parameters/function for sending custom headers as strings
    or callbacks run at send time.
    """

    server_version = "simplehttpd/" + __version__
    custom_headers = OrderedDict()

    def send_custom_headers(self, response, file=None):
        for header_type, header in self.custom_headers.items():
            if callable(header):
                header = header(response, file=file)
            self.send_header(header_type, header)

    def send_head(self):
        """Common code for GET and HEAD commands.

        This sends the response code and MIME headers.

        Return value is either a file object (which has to be copied
        to the outputfile by the caller unless the command was HEAD,
        and must be closed by the caller under all circumstances), or
        None, in which case the caller has nothing further to do.

        """
        path = self.translate_path(self.path)
        f = None
        if os.path.isdir(path):
            if not self.path.endswith('/'):
                # redirect browser - doing basically what apache does
                self.send_response(301)
                self.send_header("Location", self.path + "/")
                self.end_headers()
                return None
            for index in "index.html", "index.htm":
                index = os.path.join(path, index)
                if os.path.exists(index):
                    path = index
                    break
            else:
                return self.list_directory(path)
        ctype = self.guess_type(path)
        try:
            f = open(path, 'rb')
        except IOError:
            self.send_error(404, "File not found")
            return None
        self.send_response(200)
        self.send_header("Content-type", ctype)
        fs = os.fstat(f.fileno())
        self.send_header("Content-Length", str(fs[6]))
        self.send_header("Last-Modified", self.date_time_string(fs.st_mtime))
        self.send_custom_headers(f, file=f)
        self.end_headers()
        return f

    def list_directory(self, path):
        """Helper to produce a directory listing (absent index.html).

        Return value is either a file object, or None (indicating an
        error).  In either case, the headers are sent, making the
        interface the same as for send_head().

        """
        try:
            list = os.listdir(path)
        except os.error:
            self.send_error(404, "No permission to list directory")
            return None
        list.sort(key=lambda a: a.lower())
        r = []
        displaypath = html.escape(urllib.parse.unquote(self.path))
        r.append('<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 3.2 Final//EN">')
        r.append("<html>\n<title>Directory listing for %s</title>\n" % displaypath)
        r.append("<body>\n<h2>Directory listing for %s</h2>\n" % displaypath)
        r.append("<hr>\n<ul>\n")
        for name in list:
            fullname = os.path.join(path, name)
            displayname = linkname = name
            # Append / for directories or @ for symbolic links
            if os.path.isdir(fullname):
                displayname = name + "/"
                linkname = name + "/"
            if os.path.islink(fullname):
                displayname = name + "@"
                # Note: a link to a directory displays with @ and links with /
            r.append('<li><a href="%s">%s</a>\n'
                    % (urllib.parse.quote(linkname), html.escape(displayname)))
        r.append("</ul>\n<hr>\n</body>\n</html>\n")
        enc = sys.getfilesystemencoding()
        encoded = ''.join(r).encode(enc)
        f = io.BytesIO()
        f.write(encoded)
        f.seek(0)
        self.send_response(200)
        self.send_header("Content-type", "text/html; charset=%s" % enc)
        self.send_header("Content-Length", str(len(encoded)))
        self.send_custom_headers(f)
        self.end_headers()
        return f

def test(HandlerClass=server.BaseHTTPRequestHandler, ServerClass=server.HTTPServer, protocol="HTTP/1.0", port=8000):
    """Test the HTTP request handler class.

    This runs an HTTP server on port given.
    """
    server_address = ('', port)

    HandlerClass.protocol_version = protocol
    httpd = ServerClass(server_address, HandlerClass)

    sa = httpd.socket.getsockname()
    print("Serving HTTP on", sa[0], "port", sa[1], "...")
    try:
        httpd.serve_forever()
    except KeyboardInterrupt:
        print("\nKeyboard interrupt received, exiting.")
        httpd.server_close()
        sys.exit(0)

def run(port=None):
    if port is None:
        if sys.argv[1:]:
            port = int(sys.argv[1])
        else:
            port = 8000
    test(port=port, HandlerClass=handler)

if __name__ == '__main__':
    run()
