from distutils.core import setup
import simplehttpd

with open('README.md') as readme_f:
    long_description = readme_f.read()

kwargs = {
    'name': 'simplehttpd',
    'version': simplehttpd.__version__,
    'description': 'An extendable replacement for http.server and its SimpleHTTPRequestHandler',
    'long_description': long_description,
    'author': 'AndrewBC',
    'author_email': 'andrew.b.coleman@gmail.com',
    'url': 'https://github.com/AndrewBC/simplehttpd',
    'download_url': 'https://github.com/AndrewBC/simplehttpd/downloads',
    'license': 'MIT',
    'platforms': ['Python'],
    'classifiers': [
        'Development Status :: 2 - Pre-Alpha',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: MIT License',
        'Natural Language :: English',
        'Programming Language :: Python :: 3',
        'Topic :: Internet :: WWW/HTTP :: HTTP Servers'
        ],
    'py_modules': ['simplehttpd'],
}

setup(**kwargs)
